import UIKit

// Variables globales y dato de entrada
// El dato de entrada debe ser superior o igual a un lenght 2 caracteres
var dato: String = "AA"
var salida: String = dato
var lista_dato = [String]()
var lista_coincidencias = [String]()
var tamaño_flag: Bool = false
var tamaño = 1

//Variable auxiliares
var posicion_inicial_coincidencia: Int = 0
var posicion_final_coincidencia: Int = 0

// Constante globales memoria
let posicion_inicial: Int = 0
let lista_numeros=["1","2","3","4","5","6","7","8","9","0"]


// Creando lista de caracteres en base al dato de entrada
for char in dato {
    lista_dato.append("\(char)")
}

// Funcion para procesar coincidencias
func  procesarCoincidencia(tamaño_final: Int, string_coincidencia: String, posicion_actual: Int) {
    // print("procesando coincidencia")
    var coincidencia_construida: String = ""
    // Construyendo el string para reemplzar la coincidencia
    for _ in 1...tamaño_final {
        // print(string_coincidencia)
        coincidencia_construida = coincidencia_construida + string_coincidencia
    }
    // print(coincidencia_construida)
    var nueva_salida: String = salida.replacingOccurrences(of: coincidencia_construida, with: (String(tamaño_final))+string_coincidencia )
    // replacingOccurrences( of: coincidencia_construida, with: (String(4)) )
    //print("salida")
    if((posicion_actual+1) == (posicion_final+1)) {
        // Mostrar resultado final
        print(nueva_salida)
    }
    salida = nueva_salida
}

// Recorrer el array de entrada y compàrar con el proximo
// con sus respectivas condiciones para contar y procesar los eventos de coincidencias
let posicion_final: Int = lista_dato.count-2
print(dato)

// Agregar restriccion a la operacion el dato de entrada debe tener al menos 2 caracteres
for posicion: Int in 0...posicion_final {
    
        print(posicion)
        if(lista_dato[posicion] == lista_dato[posicion+1] ) {
            // print("coincidencia")
            if(tamaño_flag == true && (posicion+1) != (posicion_final+1)){
                //print("tamaño aumenta")
                tamaño = tamaño + 1
                // print(tamaño)
            } else if (tamaño_flag == true && (posicion+1) == (posicion_final+1) ) {
                // SALIDA
                // print("coincidencia final que arrastra evento de deteccion")
                tamaño = tamaño + 2
                // print("tamaño final " + String(tamaño))
                procesarCoincidencia(tamaño_final: tamaño , string_coincidencia: lista_dato[posicion], posicion_actual: posicion)
            } else {
                posicion_inicial_coincidencia = posicion
                //print("posicion inicial coincidencia " + String(posicion_inicial_coincidencia))
                if((posicion+1) == (posicion_final+1)){
                    // SALIDA
                    // print("logica de coincidencia final sobre un primer par")
                    tamaño = tamaño + 1
                    // print("tamaño final " + String(tamaño))
                    procesarCoincidencia(tamaño_final: tamaño , string_coincidencia: lista_dato[posicion], posicion_actual: posicion)
                }
            }
            tamaño_flag=true
            
        } else {
            // print("no coincidencia")
            if(tamaño_flag){
                // SALIDA
                posicion_final_coincidencia = posicion
                // print("posicion final coincidencia " + String(posicion_final_coincidencia))
                tamaño = tamaño + 1
                // print("tamaño final")
                // print(tamaño)
                procesarCoincidencia(tamaño_final: tamaño , string_coincidencia: lista_dato[posicion], posicion_actual: posicion)
                tamaño=1
            }
            tamaño_flag=false
        }
   
}














































