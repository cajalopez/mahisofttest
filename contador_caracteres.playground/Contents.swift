import UIKit
import Foundation


// 1 .- Separar el string en una lista de cadena de caracteres
// 2 .- Crear metodo que sustituya los fragmentos del string con caracteres repetidos
// 3 .- Publicar resultado.

var texto = TextRaro(valor_texto : "ABBBBCFL")

class TextRaro {
    
    var valor_texto: String
    var valor_resultado: String = ""
    var lista_caracteres = [String]()
    var existe_tramo_repetido: Bool = false
    
    init(valor_texto: String) {
        self.valor_texto = valor_texto
        crearLista()
        print(buscarTramoRepetido())
    }
    
    func crearLista() {
       
        for char in valor_texto {
            lista_caracteres.append("\(char)")
        }
    }

    
    // ALgoritmo que retorna la sustitucion por tramo detectado
    func buscarTramoRepetido() -> String {
 
        let posicion_maxima:Int = valor_texto.count-1
        var i: Int = 0

            while(i+1<posicion_maxima){
                
                i=i+1
                print("posicion")
                print(i)
                print(posicion_maxima)
                
                if (lista_caracteres[i]==lista_caracteres[i+1])
                {
                    
                    existe_tramo_repetido = true
                    
                    print("caracter --- "+lista_caracteres[i])
                    print("coincidencia si")
                    print("posicion inicial de incedencia")
                    print(i)
            
                    var posicion_final_coincidencia: Int = i+1
                    let posicion_inicia_coincidencia: Int = i
                    var tamaño: Int = 1
                    
                    while (lista_caracteres[posicion_inicia_coincidencia]==lista_caracteres[posicion_final_coincidencia]) {
                            posicion_final_coincidencia = posicion_final_coincidencia + 1
                            tamaño = tamaño + 1
                    }
                    
                    print("tamaño")
                    print(tamaño)
                    print("posicion final de coincidencia")
                    posicion_final_coincidencia = posicion_final_coincidencia - 1
                    print(posicion_final_coincidencia)
                    
                    var tramo_1: String = ""
                    
                    for posicion in 0...posicion_inicia_coincidencia {
        
                        if(posicion == posicion_inicia_coincidencia) {
                            tramo_1.append(String(tamaño))
                        } else {
                            tramo_1.append(lista_caracteres[posicion])
                        }
                    
                    }
                    
                    tramo_1.append(lista_caracteres[posicion_inicia_coincidencia])
                    var tramo_2: String = tramo_1
                    
                    for posicion in (posicion_final_coincidencia+1)...posicion_maxima {
                        tramo_2.append(lista_caracteres[posicion])
                    }
                    
                    // Reinicio de la lista con los caracteres procesados.
                    lista_caracteres.removeAll()
                    
                    for char in tramo_2 {
                        lista_caracteres.append("\(char)")
                    }
                    
                    print(lista_caracteres)
                    valor_texto = tramo_2
                    print(valor_texto)
                    
                    
                }
        }
        return valor_texto
    }
    
}












































