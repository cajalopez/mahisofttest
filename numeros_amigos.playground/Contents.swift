import UIKit

// Feature: Determinar si los numeros son amigos, ver condiciones en el archivo anexo
// o ticket de JIRA.
// Objetivo: Crear metodo para validar las condiciones de entrada.
// Condicion entrada: Ser numeros enteros, Positivos.

// Valores de entrada para probar el algoritmo
var a_entrada: Int = 220
var b_entrada: Int = 284

// Definir objeto amigo
let a_numero = Numero (valor : a_entrada)
let b_numero = Numero (valor : b_entrada)
validarAmistad(A : a_numero, B : b_numero)

class Numero {
    
    let valor: Int
    var lista_divisores = [Int]()
    var suma_divisores: Int  = 0
    
    init(valor: Int) {
        self.valor = valor
        crearLista()
        sumaDivisores()
    }
    
    func crearLista() {
        // Obtener lista de divisores
        for i in 1...valor {

            // Nota: el entero a procesar puede ser un numero muy
            // grande por lo cual esta tarea de procesamiento de datos
            // deberia ser delegada a backend o restringir la entrada a un mas:
            if(valor%i == 0) {
                if(i != valor) {
                    lista_divisores.append(i)
                }
            }
            
        }
    }
    
    func sumaDivisores() {
        
        var aux: Int = 0
        for i in 0 ..< lista_divisores.count {
            aux = aux + lista_divisores[i]
            // print(lista_divisores[i])
        }
        
        suma_divisores = aux
        //print(suma_divisores)
    }

}

// Metodo de validacion de amistad
func validarAmistad(A: Numero, B: Numero){
    
    if(A.valor == B.suma_divisores && B.valor == A.suma_divisores) {
        print("SON AMIGOS")
    } else {
        print("NO SON AMIGOS")
    }

}












































