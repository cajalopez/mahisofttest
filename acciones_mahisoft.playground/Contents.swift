import UIKit

// Datos de entrada se debe construir
// Arreglos siempre de la misma longitud
// un metodo para crear el diccionario: Accion(dias: Int,precio: Int) {}
let precios = [1,2,1,5,2.1,6,3]
let dias = [0,1,2,3,4,5,6]

print("VALORES ENTRADA DESDE MAHISOFT API")
print(precios)
print(dias)
print()
print("SOLUCION")
print("--------")

// Variables auxiliares
var precio_bajo=0.0
var precio_alto=0.0

// Recorrer los dias y verificar el valor de las acciones
for dia in 0...(dias.count-1){
    // print(dias[posicion])
    if(dia == 0) {
        precio_bajo = precios[dia]
        precio_alto = precios[dia]
    } else {
        // print(precios[posicion])
        // Guardar precio mas bajo
        if (precio_bajo<precios[dia]){
            } else {
               // print("nuevo precio bajo")
               // print("en posicion -> "+String(posicion)+" es "+String(precios[posicion]))
               precio_bajo = precios[dia]
               // print(precio_bajo)
            }
        // Guardar precio mas alto
        if (precio_alto>precios[dia]){
            } else {
                // print("nuevo precio alto")
                // print("en posicion -> "+String(posicion)+" es "+String(precios[posicion]))
                precio_alto = precios[dia]
                // print(precio_bajo)
            }
    }
}

// Recorrer precios y anunciar dia de compra y venta al usuario
for posicion in 0...(precios.count-1){
    if(precios[posicion]==precio_bajo){
        print("dia de compra")
        print("valor de la accion -> "+String(precios[posicion]))
        print("dia -> "+String(posicion))
        print("--------")
    }
    if(precios[posicion]==precio_alto){
        print("dia de venta")
        print("valor de la accion -> "+String(precios[posicion]))
        print("dia -> "+String(posicion))
        print("--------")
    }
}
